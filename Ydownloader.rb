require 'rubygems'
require 'open-uri'
require 'nokogiri'
require 'restclient'
require 'optparse'

YOUTUBE_URL = 'https://www.youtube.com'

OPT_STR = 'Optional input file. input.txt by default'
OUT_STR = 'Optional output file. output.txt by default'

# ARGV options
options = {}
OptionParser.new do |opts|
  options[:ipt] = false
  opts.on('-i', '--optional [INPUT]', OPT_STR) do |ipt|
    options[:input] = ipt || 'input.txt'
  end
  opts.on('-o', '--output [OUTPUT]', OUT_STR) do |opt|
    options[:output] = opt || 'output.txt'
  end
end.parse!

# No ARGV default options
options[:input] ||= 'input.txt'
options[:output] ||= 'output.txt'

# Reading input
videos = []
File.open(options[:input], 'r').each_line do |line|
  video = {}
  video[:name] = line.delete("\n").tr('ñ', 'n')
  videos << video
end

# Crawling process
videos.each do |video|
  url_get = video[:name].tr(' ', '+').delete('"')
  query = YOUTUBE_URL + "/results?search_query=#{url_get}"
  html_object = Nokogiri::HTML(open(query)).css('div.yt-lockup').first
  if html_object
    id = html_object.attributes['data-context-item-id']
    video[:url] = YOUTUBE_URL + "/watch?v=#{id}"
  end
end

# Writing and printing
out = File.open(options[:output], 'w')
videos.each do |video|
  output_line = "#{video[:url]}\n"
  out.write(output_line)
  puts output_line
end
out.close
