# README #

This tool takes a set of video names and returns a set with matching Youtube URLs. V1



---- Use instructions: ----

ruby Ydownloader.rb -i [InputFileName] -o [OutputFileName]

Both arguments are optional

 
---- Input file: ----

Write each name in separate rows. Each output row will match with this structure. An example of input is given.


---- Details: ----

It searches the input names in youtube and chooses the first element for each name (it will be theoretically the best match, but it will depend on the Youtube searching engine).